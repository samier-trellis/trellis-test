FROM python:3.10-slim-buster 

# Ensure that Python output is sent to the terminal immediately
ENV PYTHONUNBUFFERED 1

# Make workdir explicit
WORKDIR /

COPY ./manage.py ./manage.py
COPY ./trellis_test/ ./trellis_test/

COPY ./requirements.txt ./requirements.txt
RUN pip install --no-cache-dir --upgrade -r requirements.txt

# Run application as non-root user for security reasons
USER www-data 
CMD ["gunicorn", "trellis_test.wsgi:application", "--bind", "0.0.0.0:8000"]