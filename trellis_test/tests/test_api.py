# This file contains end-to-end tests for the API endpoints
import pytest
from django.test import Client
from django.urls import reverse
import json


@pytest.mark.parametrize(
    ["number", "expected"], [("44", "forty four"), ("1", "one"), ("59", "fifty nine")]
)
def test_conversion_endpoint_post(
    client: Client, number: str, expected: str
):  # apparently, just specifying client "fixture" like this works b/c pytest-django
    url = reverse(
        "api-1.0.0:num_function_post"
    )  # Django ninja default namespace is this api tag thing

    request_body = {"number": number}

    response = client.post(
        url, data=json.dumps(request_body), content_type="application/json"
    )

    assert response.status_code == 200

    resp_object = response.json()
    assert resp_object["num_in_english"] == expected


@pytest.mark.parametrize(["number", "expected"], [("3", "three"), ("19", "nineteen")])
def test_conversion_endpoint_get(client: Client, number: str, expected: str):
    url = reverse("api-1.0.0:num_function_get")

    response = client.get(f"{url}?number={number}")
    assert response.status_code == 200

    resp_object = response.json()
    assert resp_object["num_in_english"] == expected


@pytest.mark.parametrize(["alleged_number"], [("hello",), ("098",), ("fod67",), (10,)])
def test_conversion_post_validation(client: Client, alleged_number: str):
    url = reverse("api-1.0.0:num_function_post")

    request_body = {"number": alleged_number}

    response = client.post(
        url, data=json.dumps(request_body), content_type="application/json"
    )

    assert response.status_code == 422


@pytest.mark.parametrize(["alleged_number"], [("hello",), ("098",), ("fod67",)])
def test_conversion_get_validation(client: Client, alleged_number: str):
    url = reverse("api-1.0.0:num_function_get")

    response = client.get(f"{url}?number={alleged_number}")

    assert response.status_code == 422


@pytest.mark.parametrize(["input"], [("1000000000000000000000",)])
def test_conversion_get_general_err(client: Client, input: str):
    # this tests both that we get predicted error response from API endpoint
    # and that passing a number above the max that the conversion function can handle
    # actually raises an exception as opposed to simply returning an incorrect result
    url = reverse("api-1.0.0:num_function_get")

    response = client.get(f"{url}?number={input}")

    assert response.status_code == 500

    response_object = response.json()
    assert (
        "num_in_english" in response_object
        and len(response_object["num_in_english"]) == 0
    )
    assert "status" in response_object and len(response_object["status"]) > 0


@pytest.mark.parametrize(["input"], [("1000000000000000000000",)])
def test_conversion_post_general_err(client: Client, input: str):
    url = reverse("api-1.0.0:num_function_post")

    request_body = {"number": input}

    response = client.post(
        url, data=json.dumps(request_body), content_type="application/json"
    )

    assert response.status_code == 500

    response_object = response.json()
    assert (
        "num_in_english" in response_object
        and len(response_object["num_in_english"]) == 0
    )
    assert "status" in response_object and len(response_object["status"]) > 0
