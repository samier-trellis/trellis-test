import pytest

from trellis_test.logic.convert_num import (
    format_and_group_digits,
    digits_to_word,
    num_to_words,
)


@pytest.mark.parametrize(
    ["num", "expected_res"],
    [
        ("100", [[1, 0, 0]]),
        ("87", [[8, 7]]),
        ("9870", [[8, 7, 0], [9]]),
        ("108971", [[9, 7, 1], [1, 0, 8]]),
        ("1001", [[0, 0, 1], [1]]),
        ("7", [[7]]),
        ("8000001", [[0, 0, 1], [0, 0, 0], [8]]),
        ("333", [[3, 3, 3]]),
        ("123456789012", [[0, 1, 2], [7, 8, 9], [4, 5, 6], [1, 2, 3]]),
    ],
)
def test_group_digits(num, expected_res):
    assert format_and_group_digits(num) == expected_res


@pytest.mark.parametrize(
    ["digits", "place_number", "expected"],
    [
        ([9], 0, "nine"),
        ([9, 6], 0, "ninety six"),
        ([1, 1, 2], 0, "one hundred twelve"),
        ([1], 1, "one thousand"),
        ([0, 0, 1], 1, "one thousand"),
        ([0, 0, 1], 2, "one million"),
        ([0, 0, 0], 2, ""),
        ([0, 0, 0], 0, ""),
        ([0, 1, 5], 0, "fifteen"),
        ([0, 5, 0], 1, "fifty thousand"),
        ([1], 3, "one billion"),
        ([0, 2, 5], 3, "twenty five billion"),
        ([0, 1, 0], 0, "ten"),
        ([0, 2, 0], 2, "twenty million"),
        ([0, 1, 3], 0, "thirteen"),
        ([0, 1, 3], 3, "thirteen billion"),
        ([1, 0, 3], 2, "one hundred three million"),
        ([1, 0, 3], 0, "one hundred three"),
        ([2, 4], 0, "twenty four"),
    ],
)
def test_digits_to_word(digits, place_number, expected):
    assert digits_to_word(digits, place_number) == expected


@pytest.mark.parametrize(
    ["number", "expected"],
    [
        ("879", "eight hundred seventy nine"),
        ("0", "zero"),
        (
            "12345678",
            "twelve million three hundred forty five thousand six hundred seventy eight",
        ),
        ("1000", "one thousand"),
        ("1000000001", "one billion one"),
        (
            "999999999999999999999",
            "nine hundred ninety nine quintillion nine hundred ninety nine quadrillion nine hundred ninety nine trillion nine hundred ninety nine billion nine hundred ninety nine million nine hundred ninety nine thousand nine hundred ninety nine",
        ),
        ("10", "ten"),
        ("11", "eleven"),
        ("30", "thirty"),
        ("15", "fifteen"),
        ("1", "one"),
        ("100000000", "one hundred million"),
    ],
)
def test_num_to_word(number, expected):
    assert num_to_words(number) == expected
