import pytest

from trellis_test.logic.text_helpers import truncate_spaces


@pytest.mark.parametrize(
    ["input", "expected"],
    [
        ("one billion  one", "one billion one"),
        ("hello        hi", "hello hi"),
        ("People v.  Molineux, 83 N.Y.    418", "People v. Molineux, 83 N.Y. 418"),
    ],
)
def test_truncate_spaces(input, expected):
    assert truncate_spaces(input) == expected
