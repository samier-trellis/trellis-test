from functools import wraps
from django.http import HttpRequest, JsonResponse
import logging

from trellis_test.types import ConversionResponse

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.ERROR)


def handle_error(endpoint):
    """
    Decorator to wrap API endpoint functions for centralized error handling.

    If the wrapped endpoint raises an exception, this decorator captures it,
    logs the error message, and sends a standardized error response with a
    500 status code.

    Args:
        endpoint (Callable): The API endpoint function to be wrapped.

    Returns:
        Callable: The wrapped function which will handle exceptions and return
                  standardized error responses when they occur.

    Usage:
        @handle_error
        def my_endpoint(request, arg1, arg2):
            ...  # some logic that might raise an exception

    """

    @wraps(endpoint)
    def _wrapped(request: HttpRequest, *args, **kwargs):
        try:
            return endpoint(request, **kwargs)
        except Exception as e:
            logger.error(str(e))
            resp = ConversionResponse(
                status="There was an error in attempting to convert the provided number.",
                num_in_english="",
            ).dict()
            return JsonResponse(data=resp, status=500, safe=False)

    return _wrapped
