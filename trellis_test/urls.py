from django.contrib import admin
from django.urls import path
from ninja import NinjaAPI

from trellis_test.logic.convert_num import num_to_words
from trellis_test.types import ConversionInput, ConversionResponse, ConversionRequest
from trellis_test.decorators import handle_error

api = NinjaAPI(title="Number Conversion API")


@api.get("test", summary="Endpoint to be used as a simple health check.")
def test(request):
    return {"content": "hello"}


@api.get(
    "/num_to_english",
    summary="Converts number to representation in words.",
    response=ConversionResponse,
)
@handle_error
def num_function_get(request, number: ConversionInput):
    result: str = num_to_words(number)
    return ConversionResponse(status="ok", num_in_english=result)


@api.post(
    "/num_to_english",
    summary="Converts number to representation in words.",
    response=ConversionResponse,
)
@handle_error
def num_function_post(request, req_body: ConversionRequest):
    result: str = num_to_words(req_body.number)
    return ConversionResponse(status="ok", num_in_english=result)


urlpatterns = [path("admin/", admin.site.urls), path("api/v1/", api.urls)]
