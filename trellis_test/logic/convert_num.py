from typing import List
from collections import deque
from itertools import dropwhile

from trellis_test.logic.text_helpers import format_output
from trellis_test.logic.number_dicts import DIGITS, SPECIAL, PLACE_VALUE_SUFFIX


def num_to_words(num: str) -> str:
    """
    Converts a given number represented as a string into its "verbal" representation in English.

    There are three primary steps to this function:

    1. Grouping the digits; the function groups the number into chunks of three for the various place values (e.g., hundreds, thousands, millions). In the process, the digits are cast as ints.

    Example output of the grouping function: "10000" -> [[1, 0], [0, 1, 1]]

    2. These chunks are passed to another function, `digits_to_word` that does the work of transforming each chunk into its word representation (e.g., "ten thousand" and "eleven")

    3. Back in the body of this function, the chunks are reassembled into the final output.

    Args:
        num (str): The number to be converted, provided as a string.
                   In this version of the code, this is expected to be a non-negative integer.

    Returns:
        str: Representation of the input number in/as English words.

    Example:
        >>> num_to_words("12345")
        'twelve thousand three hundred forty five'
    """

    if num == "0":
        return "zero"

    if num in DIGITS:
        return DIGITS[num]

    if num in SPECIAL:
        return SPECIAL[num]

    groups: List[List[int]] = format_and_group_digits(num)

    # use deque here to reconstruct the original order
    # simply reversing a list would add another O(n) pass where n = the number of components
    components = deque([])

    for place_index, group in enumerate(groups):
        group_words = digits_to_word(group, place_index)
        components.appendleft(group_words)

    number_in_words = " ".join(components)
    return format_output(number_in_words)


def format_and_group_digits(number: str) -> List[List[int]]:
    """
    Converts string to list of ints,
    and then groups the digits in groups of three, starting from the least significant digit (rightmost).
    This is because the only incomplete group should be the head
    (e.g. the "48" in 48, 010; it wouldn't make sense to split into 480 and 10)

    The groups are returned in reverse to make their index proportional
    to the power of 10 they represent.
    """
    digits = [int(char) for char in number]
    return [digits[max(x - 3, 0) : x] for x in range(len(digits), -1, -3) if x > 0]


def digits_to_word(digits: List[int], place_number: int) -> str:
    """
    Converts a group of digits to its corresponding component of
    the English word representation for the original number.

    This is the main function in this entire flow.

    Args:
    - digits (List[int]): List of integer digits, where each integer is a single digit.
    - place_number (int): Represents the place value of the group, e.g., 0 for ones, 1 for thousands, 2 for millions, etc.

    Returns:
    - str: English word representation of the digits.

    Example:
    digits_to_word([1, 2, 3], 1) -> "one hundred twenty three thousand"

    The function is able to correctly append the "thousand" because of the argument place_number=1, indicating that this group represents the "thousands" segment of the broader number.
    """

    # first, drop preceding zeroes. they are nullities; they do not affect the
    # resulting word. E.g. 001 in the thousands place is simply "one thousand"
    # This is constant time because max length of digits = 3
    digits = list(dropwhile(lambda d: d == 0, digits))
    if len(digits) == 0:
        return ""

    digits = digits[
        ::-1
    ]  # because of pattern matching, the reversal is not strictly necessary
    # however, it does make the pattern matching more readable and is constant time anyways

    # prefer to use iterable when concatenating str because python strings are immutable
    # and therefore string concat is O(k) where k = len of combined str
    # thus, str += ends up being a trap that often results
    # in (basically) O(n^2) algorithmic runtime
    # probably doesn't matter for this function anyways, as max length of string
    # is within pretty small bounds, but doesn't hurt to make this optimization.
    res = []
    main_body: str

    # use pattern matching to create string based on how many digits are present.
    # unfortunately, when the tens place is occupied by '1', the case is special.
    match digits:
        case [ones]:
            main_body = DIGITS[ones]
        case [ones, 1]:
            main_body = SPECIAL[10 + ones]
        case [ones, tens]:
            main_body = f"{SPECIAL[tens]} {DIGITS[ones]}"
        case [ones, 1, hundreds]:
            main_body = f"{DIGITS[hundreds]} hundred {SPECIAL[10 + ones]}"
        case [ones, tens, hundreds]:
            main_body = f"{DIGITS[hundreds]} hundred {SPECIAL[tens]} {DIGITS[ones]}"

    res.append(main_body)

    if place_number > 0:
        res.append(f"{PLACE_VALUE_SUFFIX[place_number]}")

    return format_output(" ".join(res))
