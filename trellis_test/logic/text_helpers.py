import re


def truncate_spaces(input: str) -> str:
    # various weird "off by one" spacing things occur and this is the most straight-forward way of handling them
    return re.sub(r"[ \s]{2,}", r" ", input)


def format_output(number_words: str) -> str:
    return truncate_spaces(number_words).strip()
