from ninja import Schema
from pydantic import constr, Field
from typing import Literal


# Specifies type constraints for number input.
# Numbers must consist only of digits, be non-empty, and must be passed as strings, not ints.
ConversionInput = constr(min_length=1, regex=r"^[1-9]+[0-9]*$", strict=True)


class ConversionRequest(Schema):
    number: ConversionInput = Field(
        description="Number to convert to words. For example, if number == '95', output will be 'ninety five' \n Leading zeros are not permitted; e.g. '0095' would not be permitted. \n The max possible input is 999999999999999999999 (nine hundred ninety nine quintillion nine hundred ninety nine quadrillion nine hundred ninety nine trillion nine hundred ninety nine billion nine hundred ninety nine million nine hundred ninety nine thousand nine hundred ninety nine) \n Per the assignment specification, the number must be formatted as a string. Malformed request parameters, such as numbers with leading zeros, plain words (e.g. 'apple'), or alphanumerics (e.g. 'd4fna2') will result in an error response. Such a response will have HTTP status code 422, and will come directly from Pydantic's validation process.",
        example="345",
    )


class ConversionResponse(Schema):
    status: str = Field(
        description="Status of the conversion attempt; this will say'ok' if the conversion succeeded. If the conversion failed, the error will be logged and this field will show a generic message that can be optionally shown to users."
    )
    num_in_english: str = Field(
        description="The actual result of the conversion (e.g. 'one hundred one'). This field will be empty in the event of a server-side error.",
        example="seventy seven",
    )
