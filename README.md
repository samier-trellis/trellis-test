# Trellis Test

Trellis test assignment submission from Samier Saeed (https://github.com/ss108)

## Installation

There are 2 options to run the project locally:

1. Use one's preferred Python virtualenv with one's preferred Python package management flow
2. Use Docker

Either way, upon start, a local sqlite db will automatically be created.

### Flow 1 (Local virtual environment)

Create and activate the preferred virtual environment (either `venv` or `virtualenv`).
Requirements can then be installed using either `pdm install` or, if one doesn't want to use PDM,
using `pip install -r requirements.txt` (the pre-commit hook keeps the requirements file updated)

### Flow 2 (Docker)

Assuming the latest version of Docker is installed, one can run `docker compose up --build -d`
and the project will be running on localhost port 8000.

Note that this setup is, at the moment, suboptimal for making changes because
the compose file lacks a volume mount (and thus the code in the container won't be up-to-date with local changes without rebuilding)

## Running Tests

Running `pytest` from the root dir will run all unit and end-to-end tests.

## Documentation Site

The automatically-generated documentation can be viewed at: /api/v1/docs

All the information is there, but the formatting leaves a bit to be desired. Consuming developers are recommended, therefore, to read the code comments directly if possible.
